<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];

var_dump(getOddNumbers($numbers));
print_r(getOddNumbers($numbers));

function getOddNumbers($list) {
    $oddNumbers = [];
    foreach ($list as $element) {
        if ($element % 2 !== 0) {
            array_push($oddNumbers, $element);
        }
    }
    return $oddNumbers;
}

