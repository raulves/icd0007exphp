// Siin on näide kuidas me saame sõnastikus olevaid elemente kokku üheks stringiks panna ning need listi panna .
// Samuti näeme, kuidas string faili kirjutada
// PHP_EOL on reavahetus

<?php

use function Sodium\add;

$file = 'data/grades.txt';

// See on sõnastik ehk dictionary
$additional_data = ['history' => 5, 'chemistry' => 2];

$stringList = [];
foreach ($additional_data as $key => $value) {
    array_push($stringList, join(';', [$key, $value]));
    print "Key is $key and value is $value" . PHP_EOL;
}
// Kirjutame tulemuse faili. Faili kirjutamise teeme loopist väljas, kuna siis peame faili avama ainult ühe korra.
// Kood on siis kiirem.
file_put_contents($file, join(PHP_EOL, $stringList) . PHP_EOL, FILE_APPEND);

print_r($stringList);