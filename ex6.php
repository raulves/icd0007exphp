// Failist lugemise näide

<?php

// Faili asikoht
$fileLocation = 'data/grades.txt';

// Loeb failist andmed (read), moodustab listi
$lines = file($fileLocation);
print_r($lines);

$sum = 0;
foreach ($lines as $line) {
    // Võtame failist ühe rea (string) ning seejärel explode meetodiga murrame ta lahti delimiter abil.
    // Moodustunud elemendid (stringid) lisame listi.

    $list = explode(';', trim($line));
    $sum += $list[1];

}

print $sum / count($lines);

