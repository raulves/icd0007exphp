<?php

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];

var_dump(isInList($numbers, 5));
var_dump(isInList($numbers, 2));



function isInList($list, $elementToBeFound) {
    foreach ($list as $element) {
        if ($element === $elementToBeFound) {
            return true;
        }
    }
    return false;
}

