<?php

for ($i = 1; $i <= 15; $i++) {
    if ($i % 3 === 0 && $i % 5 === 0) {
        print "FizzBuzz" . PHP_EOL;
    } elseif ($i % 3 === 0) {
        print "Fizz" .PHP_EOL;
    } elseif ($i % 5 === 0) {
        print "Buzz" .PHP_EOL;
    } else {
        print $i .PHP_EOL;
    }

}

foreach (range(1, 15) as $number) {
    print $number . PHP_EOL;
}

